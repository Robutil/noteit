package me.robbit.noteit;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Scroller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;

import me.robbit.noteit.core.StorageManager;
import me.robbit.noteit.interfaces.ToolbarUpdater;

public class WriterFragment extends Fragment {

    private EditText editText;

    private ToolbarUpdater toolbarUpdater;
    private StorageManager storageManager;
    private final String filename = "test.md";

    public WriterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FrameLayout frameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_writer, container, false);

        editText = frameLayout.findViewById(R.id.writer_edit_text_1);
        //TODO: put EditText in ScrollView for accelerated scrolling
        editText.setScroller(new Scroller(Objects.requireNonNull(getContext()).getApplicationContext()));
        editText.setVerticalScrollBarEnabled(true);
        editText.setMovementMethod(new ScrollingMovementMethod());

        storageManager = new StorageManager(Objects.requireNonNull(getActivity()).getApplicationContext());

        if (storageManager.listDir().contains(filename)) {
            try {
                editText.setText(storageManager.read(filename));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        if (toolbarUpdater != null) {
            toolbarUpdater.setName("test.md");
        }

        return frameLayout;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarUpdater) {
            toolbarUpdater = (ToolbarUpdater) context;
            toolbarUpdater.setName("test.md");
        }
    }

    @Override
    public void onPause() {
        try {
            storageManager.write(filename, editText.getText().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        toolbarUpdater = null;
    }
}
