package me.robbit.noteit.core;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class StorageManager {
    private Context context;

    public StorageManager(Context context) {
        this.context = context;
    }

    public String joinPath(String folder, String path) {
        return folder + File.separator + path;
    }

    private String relativeToAbsolutePath(String relativePath) {
        return context.getFilesDir().getAbsolutePath() + File.separator + relativePath;
    }

    public List<String> listDir() {
        return listDir("");
    }

    public List<String> listDir(String relativePath) {
        File targetDir = new File(relativeToAbsolutePath(relativePath));
        return Arrays.asList(targetDir.list());
    }

    public String read(String filename) throws FileNotFoundException {
        FileInputStream fis = new FileInputStream(new File(relativeToAbsolutePath(filename)));
        Scanner scanner = new Scanner(fis);
        StringBuilder builder = new StringBuilder();
        while (scanner.hasNext()) {
            builder.append(scanner.nextLine()).append('\n');
        }
        return builder.toString();
    }

    public void write(String filename, String data) throws IOException {
        write(filename, data, false);
    }

    public void write(String filename, String data, boolean append) throws IOException {
        FileOutputStream fos = new FileOutputStream(new File(relativeToAbsolutePath(filename)), append);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fos);
        outputStreamWriter.write(data);
        outputStreamWriter.flush();
        outputStreamWriter.close();
    }

    public boolean isDir(String relativePath) {
        File target = new File(relativeToAbsolutePath(relativePath));
        return target.isDirectory();
    }

    public boolean createFile(String relativePath) throws IOException {
        File file = new File(relativeToAbsolutePath(relativePath));
        return file.createNewFile();
    }

    public boolean createDir(String relativePath) throws SecurityException {
        File dir = new File(relativeToAbsolutePath(relativePath));
        return dir.mkdir();
    }

    public boolean deleteFile(String filename) {
        return context.deleteFile(filename);
    }
}
