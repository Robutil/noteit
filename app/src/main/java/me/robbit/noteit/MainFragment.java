package me.robbit.noteit;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.robbit.noteit.interfaces.ToolbarUpdater;

public class MainFragment extends Fragment {

    private final String TOOLBAR_TEXT = "NoteIt";
    private ToolbarUpdater toolbarUpdater;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        if (toolbarUpdater != null) {
            toolbarUpdater.setName(TOOLBAR_TEXT);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarUpdater) {
            toolbarUpdater = (ToolbarUpdater) context;
            toolbarUpdater.setName(TOOLBAR_TEXT);
        }
    }
}
