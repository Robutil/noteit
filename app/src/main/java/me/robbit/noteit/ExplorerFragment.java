package me.robbit.noteit;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import me.robbit.noteit.core.StorageManager;
import me.robbit.noteit.interfaces.ToolbarUpdater;

import static android.R.layout.simple_list_item_1;


public class ExplorerFragment extends Fragment {

    private MainActivity callback;
    private ToolbarUpdater toolbarUpdater;

    private StorageManager storageManager;
    private ListView listView;
    private Context context;

    private String currentFolder = "";

    public ExplorerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FrameLayout frameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_explorer, container, false);

        listView = frameLayout.findViewById(R.id.fragment_explorer_list);
        context = Objects.requireNonNull(getContext()).getApplicationContext();
        storageManager = new StorageManager(context);

//        storageManager.createDir("test_directory");
        displayContents(storageManager.listDir());

        return frameLayout;
    }

    private List<String> filterContents(List<String> contents) {
        ArrayList<String> filteredContents = new ArrayList<>();
        for (String item : contents) {
            if (!item.startsWith(".")) filteredContents.add(item);
        }
        return filteredContents;
    }

    private void displayContents(List<String> contents) {
        if (toolbarUpdater != null) {
            String name = currentFolder.equals("") ?
                    storageManager.joinPath("", currentFolder) : currentFolder;
            toolbarUpdater.setName(name);
        }

        contents = filterContents(contents);
        ArrayAdapter arrayAdapter = new ArrayAdapter<>(context, simple_list_item_1, contents);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                if (storageManager.isDir(item)) {
                    currentFolder = storageManager.joinPath(currentFolder, item);
                    displayContents(storageManager.listDir(currentFolder));
                } else if (callback != null) {
                    callback.launchFragment(new WriterFragment(), "WriterFragment");
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            callback = (MainActivity) context;
        }

        if (context instanceof ToolbarUpdater) {
            toolbarUpdater = (ToolbarUpdater) context;
        }

    }
}
